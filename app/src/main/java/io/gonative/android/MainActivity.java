package io.gonative.android;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.os.PowerManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.security.keystore.UserNotAuthenticatedException;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.facebook.applinks.AppLinkData;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.squareup.seismic.ShakeDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import io.gonative.android.library.AppConfig;


public class MainActivity extends AppCompatActivity implements Observer,
        SwipeRefreshLayout.OnRefreshListener,
        LeanWebView.OnSwipeListener,
        ShakeDetector.Listener {
    public static final String webviewCacheSubdir = "webviewAppCache";
    private static final String webviewDatabaseSubdir = "webviewDatabase";
	private static final String TAG = MainActivity.class.getName();
    public static final String INTENT_TARGET_URL = "targetUrl";
    public static final String EXTRA_WEBVIEW_WINDOW_OPEN = "io.gonative.android.MainActivity.Extra.WEBVIEW_WINDOW_OPEN";
	public static final int REQUEST_SELECT_FILE = 100;
    private static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 101;
    private static final int REQUEST_PERMISSION_GEOLOCATION = 102;
    private static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 103;
    private static final int REQUEST_PHONE_CALL = 1;
    private static final int REQUEST_PERMISSION_GENERIC = 199;
    private static final int REQUEST_WEBFORM = 300;
    public static final int REQUEST_WEB_ACTIVITY = 400;
    private static final float ACTIONBAR_ELEVATION = 12.0f;

    private GoNativeWebviewInterface mWebview;
    private View webviewOverlay;
    boolean isPoolWebview = false;
    private Stack<String> backHistory = new Stack<>();
    private String initialUrl;

	private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> uploadMessageLP;
    private Uri directUploadImageUri;
	private DrawerLayout mDrawerLayout;
	private View mDrawerView;
	private ExpandableListView mDrawerList;
    private ProgressBar mProgress;
    private Dialog splashDialog;
    private boolean splashDismissRequiresForce;
    private MySwipeRefreshLayout swipeRefresh;
    private RelativeLayout fullScreenLayout;
    private JsonMenuAdapter menuAdapter = null;
	private ActionBarDrawerToggle mDrawerToggle;
    private PagerSlidingTabStrip slidingTabStrip;
    private ImageView navigationTitleImage;
	private ConnectivityManager cm = null;
    private ProfilePicker profilePicker = null;
    private TabManager tabManager;
    private ActionManager actionManager;
    private boolean isRoot;
    private float hideWebviewAlpha = 0.0f;
    private boolean isFirstHideWebview = true;
    private boolean webviewIsHidden = false;
    private int urlLevel = -1;
    private int parentUrlLevel = -1;
    private Handler handler = new Handler();
    private Runnable statusChecker = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkReadyStatus();
                }
            });
            handler.postDelayed(statusChecker, 100); // 0.1 sec
        }
    };
    private ShakeDetector shakeDetector = new ShakeDetector(this);
    private FileDownloader fileDownloader = new FileDownloader(this);
    private FileWriterSharer fileWriterSharer;
    private boolean startedLoading = false; // document readystate checker
    private LoginManager loginManager;
    private RegistrationManager registrationManager;
    private ConnectivityChangeReceiver connectivityReceiver;
    private BroadcastReceiver oneSignalStatusChangedReceiver;
    private BroadcastReceiver navigationTitlesChangedReceiver;
    private BroadcastReceiver navigationLevelsChangedReceiver;
    protected String postLoadJavascript;
    protected String postLoadJavascriptForRefresh;
    private Stack<Bundle>previousWebviewStates;
    private GeolocationPermissionCallback geolocationPermissionCallback;
    private ArrayList<PermissionsCallbackPair> pendingPermissionRequests = new ArrayList<>();
    private ArrayList<Intent> pendingStartActivityAfterPermissions = new ArrayList<>();
    private String connectivityCallback;
    private String connectivityOnceCallback;
    private PhoneStateListener phoneStateListener;
    private SignalStrength latestSignalStrength;
    private String phonenumber;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    private String mode;        // can be either login or reg
    private String userid;
    private String cuid;
    private String password;
    private static final String ENCRYPTED_PASS_SHARED_PREF_KEY = "ENCRYPTED_USERID_SHARED_PREF_KEY";
    private static final String ENCRYPTED_CUID_SHARED_PREF_KEY = "ENCRYPTED_CUID_SHARED_PREF_KEY";
    private static final String ENCRYPTED_USERID_SHARED_PREF_KEY = "ENCRYPTED_PASSWORD_SHARED_PREF_KEY";
    private static final String CUMOBILE_HELPER = "FingerPrintAuthHelper";
    private static final String LAST_USED_IV_SHARED_PREF_KEY = "IV_SHAREDPREF_KEY";


    private static final char[] hexCode = "0123456789ABCDEF".toCharArray();

    public String encodeBytes(byte[] data) {
        StringBuilder r = new StringBuilder(data.length*2);
        for ( byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }


    private void saveIv(byte[] iv) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        String string = encodeBytes(iv);
        edit.putString(LAST_USED_IV_SHARED_PREF_KEY, string);
        edit.commit();
    }


    private byte[] getLastIv() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences != null) {
            String ivString = sharedPreferences.getString(LAST_USED_IV_SHARED_PREF_KEY, null);

            if (ivString != null) {
                return decodeBytes(ivString);
            }
        }
        return null;
    }
    private void saveEncryptedUserId(String userId) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putString(ENCRYPTED_PASS_SHARED_PREF_KEY, userId);
        edit.commit();
    }

    private void saveEncryptedCuId(String cuid) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putString(ENCRYPTED_CUID_SHARED_PREF_KEY, cuid);
        edit.commit();
    }


    private void saveEncryptedPassword(String password) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putString(ENCRYPTED_USERID_SHARED_PREF_KEY, password);
        edit.commit();
    }
    private String getSavedEncryptedUserId() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences != null) {
            return sharedPreferences.getString(ENCRYPTED_PASS_SHARED_PREF_KEY, null);
        }
        return null;
    }


    private String getSavedEncryptedcuId() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences != null) {
            return sharedPreferences.getString(ENCRYPTED_CUID_SHARED_PREF_KEY, null);
        }
        return null;
    }


    private String getSavedEncryptedPassword() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences != null) {
            return sharedPreferences.getString(ENCRYPTED_USERID_SHARED_PREF_KEY, null);
        }
        return null;
    }

    private SharedPreferences getSharedPreferences() {
        Context context = getApplicationContext();
        return context.getSharedPreferences(CUMOBILE_HELPER, 0);
    }


    private byte[] decodeBytes(String s) {
        final int len = s.length();

        // "111" is not a valid hex encoding.
        if( len%2 != 0 )
            throw new IllegalArgumentException("hexBinary needs to be even-length: "+s);

        byte[] out = new byte[len/2];

        for( int i=0; i<len; i+=2 ) {
            int h = hexToBin(s.charAt(i  ));
            int l = hexToBin(s.charAt(i+1));
            if( h==-1 || l==-1 )
                throw new IllegalArgumentException("contains illegal character for hexBinary: "+s);

            out[i/2] = (byte)(h*16+l);
        }

        return out;
    }

    private static int hexToBin( char ch ) {
        if( '0'<=ch && ch<='9' )    return ch-'0';
        if( 'A'<=ch && ch<='F' )    return ch-'A'+10;
        if( 'a'<=ch && ch<='f' )    return ch-'a'+10;
        return -1;
    }

    private static byte[] fromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    @TargetApi(23)
    private void generateSecretKey(KeyGenParameterSpec keyGenParameterSpec) {
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            keyGenerator.init(keyGenParameterSpec);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        keyGenerator.generateKey();
    }

    @TargetApi(23)

    private SecretKey getSecretKey() {
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        // Before the keystore can be accessed, it must be loaded.
        try {
            keyStore.load(null);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            return ((SecretKey)keyStore.getKey("cuMobileKey", null));
        } catch (KeyStoreException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Cipher getCipher() {
        try {
            return Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
	protected void onCreate(Bundle savedInstanceState) {
        final AppConfig appConfig = AppConfig.getInstance(this);
        GoNativeApplication application = (GoNativeApplication)getApplication();

        setScreenOrientationPreference();



        if (appConfig.keepScreenOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        this.hideWebviewAlpha  = appConfig.hideWebviewAlpha;

        super.onCreate(savedInstanceState);

        isRoot = getIntent().getBooleanExtra("isRoot", true);
        parentUrlLevel = getIntent().getIntExtra("parentUrlLevel", -1);

        if (isRoot) {
            // Splash screen stuff
            boolean isFromLauncher = getIntent().hasCategory(Intent.CATEGORY_LAUNCHER);
            // FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY does not seem to be set when it should
            // for some devices. I have yet to find a good workaround.
            boolean isFromRecents = (getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0;
            boolean noSplash = getIntent().getBooleanExtra("noSplash", false);

            if (!noSplash && isFromLauncher && !isFromRecents) {
                showSplashScreen(appConfig.showSplashMaxTime, appConfig.showSplashForceTime);
            }

            // html5 app cache (manifest)
            File cachePath = new File(getCacheDir(), webviewCacheSubdir);
            if (!cachePath.mkdirs()) {
                Log.v(TAG, "cachePath " + cachePath.toString() + " exists");
            }
            File databasePath = new File(getCacheDir(), webviewDatabaseSubdir);
            if (databasePath.mkdirs()) {
                Log.v(TAG, "databasePath " + databasePath.toString() + " exists");

            }

            // url inspector
            UrlInspector.getInstance().init(this);

            // Register launch
            ConfigUpdater configUpdater = new ConfigUpdater(this);
            configUpdater.registerEvent();

            // registration service
            this.registrationManager = application.getRegistrationManager();
        }

        this.loginManager = application.getLoginManager();

        this.fileWriterSharer = new FileWriterSharer(this);

        // webview pools
        application.getWebViewPool().init(this);

		cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        if (isRoot && AppConfig.getInstance(this).showNavigationMenu)
	    	setContentView(R.layout.activity_gonative);
        else
            setContentView(R.layout.activity_gonative_nonav);


        mProgress = findViewById(R.id.progress);
        this.fullScreenLayout = findViewById(R.id.fullscreen);

        swipeRefresh = findViewById(R.id.swipe_refresh);
        swipeRefresh.setEnabled(appConfig.pullToRefresh);
        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.setCanChildScrollUpCallback(new MySwipeRefreshLayout.CanChildScrollUpCallback() {
            @Override
            public boolean canSwipeRefreshChildScrollUp() {
                return mWebview.getScrollY() > 0;
            }
        });
        if (appConfig.pullToRefreshColor != null) {
            swipeRefresh.setColorSchemeColors(appConfig.pullToRefreshColor);
        }

        this.webviewOverlay = findViewById(R.id.webviewOverlay);
        this.mWebview = findViewById(R.id.webview);
        setupWebview(this.mWebview);
        if (appConfig.swipeGestures) {
            this.mWebview.setOnSwipeListener(this);
        }

        // profile picker
        if (isRoot && AppConfig.getInstance(this).showNavigationMenu) {
            Spinner profileSpinner = findViewById(R.id.profile_picker);
            profilePicker = new ProfilePicker(this, profileSpinner);

            Spinner segmentedSpinner = findViewById(R.id.segmented_control);
            new SegmentedController(this, segmentedSpinner);
        }

		// to save webview cookies to permanent storage
		CookieSyncManager.createInstance(getApplicationContext());
		
		// proxy cookie manager for httpUrlConnection (syncs to webview cookies)
        CookieHandler.setDefault(new WebkitCookieManagerProxy());


        this.postLoadJavascript = getIntent().getStringExtra("postLoadJavascript");
        this.postLoadJavascriptForRefresh = this.postLoadJavascript;

        this.previousWebviewStates = new Stack<>();

        // tab navigation
        ViewPager pager = findViewById(R.id.view_pager);
        this.slidingTabStrip = findViewById(R.id.tabs);
        this.tabManager = new TabManager(this, pager);
        pager.setAdapter(this.tabManager);
        this.slidingTabStrip.setViewPager(pager);
        this.slidingTabStrip.setTabClickListener(this.tabManager);

        // custom colors
        if (appConfig.tabBarBackgroundColor != null)
            this.slidingTabStrip.setBackgroundColor(appConfig.tabBarBackgroundColor);
        if (appConfig.tabBarTextColor != null)
            this.slidingTabStrip.setTextColor(appConfig.tabBarTextColor);
        if (appConfig.tabBarIndicatorColor != null)
            this.slidingTabStrip.setIndicatorColor(appConfig.tabBarIndicatorColor);
        hideTabs();

        if (!appConfig.showActionBar && getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        // actions in action bar
        this.actionManager = new ActionManager(this);

        Intent intent = getIntent();
        // load url
        String url = getUrlFromIntent(intent);

        if (url == null && savedInstanceState != null) url = savedInstanceState.getString("url");
        if (url == null && isRoot) url = new ConfigPreferences(this).getInitialUrl();
        if (url == null && isRoot) url = appConfig.initialUrl;
        // url from intent (hub and spoke nav)
        if (url == null) url = intent.getStringExtra("url");

        if (url != null) {
            this.initialUrl = url;
            this.mWebview.loadUrl(url);
        } else if (intent.getBooleanExtra(EXTRA_WEBVIEW_WINDOW_OPEN, false)){
            // no worries, loadUrl will be called when this new web view is passed back to the message
        } else {
            Log.e(TAG, "No url specified for MainActivity");
        }

        if (isRoot && appConfig.facebookEnabled) {
            AppLinkData.fetchDeferredAppLinkData(this, new AppLinkData.CompletionHandler() {
                @Override
                public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                    if (appLinkData == null) return;
                    Uri uri = appLinkData.getTargetUri();
                    if (uri == null) return;
                    String url;
                    if (uri.getScheme().endsWith(".http") || uri.getScheme().endsWith(".https")) {
                        Uri.Builder builder = uri.buildUpon();
                        if (uri.getScheme().endsWith(".https")) {
                            builder.scheme("https");
                        } else if (uri.getScheme().endsWith(".http")) {
                            builder.scheme("http");
                        }
                        url = builder.build().toString();
                    } else {
                        url = uri.toString();
                    }
                    if (url != null) {
                        final String finalUrl = url;
                        new Handler(MainActivity.this.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                mWebview.loadUrl(finalUrl);
                            }
                        });
                    }
                }
            });
        }

        if (isRoot && appConfig.showNavigationMenu) {
            // do the list stuff
            mDrawerLayout = findViewById(R.id.drawer_layout);
            mDrawerView = findViewById(R.id.left_drawer);
            mDrawerList = findViewById(R.id.drawer_list);

            // set shadow
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    R.string.drawer_open, R.string.drawer_close){
                //Called when a drawer has settled in a completely closed state.
                public void onDrawerClosed(View view) {
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }

                //Called when a drawer has settled in a completely open state.
                public void onDrawerOpened(View drawerView) {
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            };
            mDrawerLayout.addDrawerListener(mDrawerToggle);

            setupMenu();

            // update the menu
            if (appConfig.loginDetectionUrl != null) {
                this.loginManager.addObserver(this);
            }
        }

		if (getSupportActionBar() != null) {
            if (!isRoot || appConfig.showNavigationMenu) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            showLogoInActionBar(appConfig.shouldShowNavigationTitleImageForUrl(url));
        }

        // style sidebar
        if (mDrawerView != null && AppConfig.getInstance(this).sidebarBackgroundColor != null) {
            mDrawerView.setBackgroundColor(AppConfig.getInstance(this).sidebarBackgroundColor);
        }

        this.oneSignalStatusChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (GoNativeApplication.ONESIGNAL_STATUS_CHANGED_MESSAGE.equals(intent.getAction())) {
                    sendOneSignalInfo();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(this.oneSignalStatusChangedReceiver,
                new IntentFilter(GoNativeApplication.ONESIGNAL_STATUS_CHANGED_MESSAGE));

        // respond to navigation titles processed
        this.navigationTitlesChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (AppConfig.PROCESSED_NAVIGATION_TITLES.equals(intent.getAction())) {
                    String url = mWebview.getUrl();
                    if (url == null) return;
                    String title = titleForUrl(url);
                    if (title != null) {
                        setTitle(title);
                    } else {
                        setTitle(R.string.app_name);
                        //setTitle("cuMobile");
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(this.navigationTitlesChangedReceiver,
            new IntentFilter(AppConfig.PROCESSED_NAVIGATION_TITLES));

        this.navigationLevelsChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (AppConfig.PROCESSED_NAVIGATION_LEVELS.equals(intent.getAction())) {
                    String url = mWebview.getUrl();
                    if (url == null) return;
                    int level = urlLevelForUrl(url);
                    setUrlLevel(level);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(this.navigationLevelsChangedReceiver,
                new IntentFilter(AppConfig.PROCESSED_NAVIGATION_LEVELS));


        // This code handles the callback from the biometric prompt used to display fingerprint login
        // APP-31

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(MainActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                //Toast.makeText(getApplicationContext(),
                //        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                //        .show();

                String js = "gonative_cancelled_biometric()";
                runJavascript(js);
            }

            @TargetApi(19)
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                if (mode.equals("reg"))
                {
                    String credentials = cuid + "|-|" + userid + "|-|" + password;

                    byte[] encryptedInfo = new byte[0];
                    try {
                        encryptedInfo = result.getCryptoObject().getCipher().doFinal(
                                credentials.getBytes(StandardCharsets.ISO_8859_1));
                                //userid.getBytes(Charset.defaultCharset()));
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    }
                    Log.d("MY_APP_TAG", "Encrypted information: " +
                            Arrays.toString(encryptedInfo));

                    saveEncryptedUserId(new String(encryptedInfo, StandardCharsets.ISO_8859_1));



                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Fingerprint registration succeeded!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();

                    String js = "gonative_enrolled_biometric()";
                    runJavascript(js);
                }
                else if(mode.equals("login"))
                {
                    String savedid = getSavedEncryptedUserId();
                    byte[] decodedUserId = savedid.getBytes(StandardCharsets.ISO_8859_1);
                    Cipher cipher = result.getCryptoObject().getCipher();
                    SecretKey secretKey = getSecretKey();

                    byte[] lastIv = getLastIv();
                    String decryptedUserId ="";
                    try {
                      byte[] res = cipher.doFinal(decodedUserId);
                        try {
                            decryptedUserId = new String(res,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    }

                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Fingerprint authentication succeeded!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();

                    Context context = getApplicationContext();
                    String js = "gonative_loggedin_biometric('" + decryptedUserId + "','" +Installation.id(context) + "')";
                    runJavascript(js);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
               // Toast.makeText(getApplicationContext(), "Authentication failed",
               //         Toast.LENGTH_SHORT)
               //         .show();
            }
        });




    }

    private String getUrlFromIntent(Intent intent) {
        if (intent == null) return null;
        // first check intent in case it was created from push notification
        String targetUrl = intent.getStringExtra(INTENT_TARGET_URL);
        if (targetUrl != null && !targetUrl.isEmpty()){
            return targetUrl;
        }

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            if (uri != null && (uri.getScheme().endsWith(".http") || uri.getScheme().endsWith(".https"))) {
                Uri.Builder builder = uri.buildUpon();
                if (uri.getScheme().endsWith(".https")) {
                    builder.scheme("https");
                } else if (uri.getScheme().endsWith(".http")) {
                    builder.scheme("http");
                }
                return builder.build().toString();
            } else {
                return intent.getDataString();
            }
        }

        return null;
    }

    protected void onPause() {
        super.onPause();
        stopCheckingReadyStatus();
        this.mWebview.onPause();

        // unregister connectivity
        if (this.connectivityReceiver != null) {
            unregisterReceiver(this.connectivityReceiver);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().flush();
        }

        shakeDetector.stop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AppConfig.getInstance(this).oneSignalEnabled) {
            OneSignal.clearOneSignalNotifications();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mWebview.onResume();

        retryFailedPage();
        // register to listen for connectivity changes
        this.connectivityReceiver = new ConnectivityChangeReceiver();
        registerReceiver(this.connectivityReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        // check login status
        this.loginManager.checkLogin();

        if (AppConfig.getInstance(this).shakeToClearCache) {
            SensorManager sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
            shakeDetector.start(sensorManager);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isDeviceLocked(getApplicationContext())){
            runJavascript("Logoff()");
        }

        if (isRoot) {
            //runJavascript("Logoff()");
            if (AppConfig.getInstance(this).clearCache) {
                this.mWebview.clearCache(true);
            }
        }
    }

    public static boolean isDeviceLocked(Context context) {
        boolean isLocked = false;

        // First we check the locked state
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean inKeyguardRestrictedInputMode = keyguardManager.inKeyguardRestrictedInputMode();

        if (inKeyguardRestrictedInputMode) {
            isLocked = true;

        } else {
            // If password is not set in the settings, the inKeyguardRestrictedInputMode() returns false,
            // so we need to check if screen on for this case

            PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                isLocked = !powerManager.isInteractive();
            } else {
                //noinspection deprecation
                isLocked = !powerManager.isScreenOn();
            }
        }

        //Loggi.d(String.format("Now device is %s.", isLocked ? "locked" : "unlocked"));
        return isLocked;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //runJavascript("Logoff()");
        // destroy webview
        if (this.mWebview != null) {
            this.mWebview.stopLoading();
            // must remove from view hierarchy to destroy
            ViewGroup parent = (ViewGroup) this.mWebview.getParent();
            if (parent != null) {
                parent.removeView((View)this.mWebview);
            }
            if (!this.isPoolWebview) this.mWebview.destroy();
        }

        this.loginManager.deleteObserver(this);

        if (this.oneSignalStatusChangedReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.oneSignalStatusChangedReceiver);
        }
        if (this.navigationTitlesChangedReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.navigationTitlesChangedReceiver);
        }
        if (this.navigationLevelsChangedReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.navigationLevelsChangedReceiver);
        }
    }

    private void retryFailedPage() {
        // skip if webview is currently loading
        if (this.mWebview.getProgress() < 100) return;

        // skip if webview has a page loaded
        String currentUrl = this.mWebview.getUrl();
        if (currentUrl != null && !currentUrl.equals("file:///android_asset/offline.html")) return;

        // skip if there is nothing in history
        if (this.backHistory.isEmpty()) return;

        // skip if no network connectivity
        if (this.isDisconnected()) return;

        // finally, retry loading the page
        this.loadUrl(this.backHistory.pop());
    }

    protected void onSaveInstanceState (Bundle outState) {
        outState.putString("url", mWebview.getUrl());
        outState.putInt("urlLevel", urlLevel);
        super.onSaveInstanceState(outState);
    }

    public void addToHistory(String url) {
        if (url == null) return;

        if (this.backHistory.isEmpty() || !this.backHistory.peek().equals(url)) {
            this.backHistory.push(url);
        }

        checkNavigationForPage(url);

        // this is a little hack to show the webview after going back in history in single-page apps.
        // We may never get onPageStarted or onPageFinished, hence the webview would be forever
        // hidden when navigating back in single-page apps. We do, however, get an updatedHistory callback.
        showWebview(0.3);
    }

    @Override
    public void hearShake() {
        clearWebviewCache();
        Toast.makeText(this, R.string.cleared_cache, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSwipeLeft() {
    }

    @Override
    public void onSwipeRight() {
        if (!AppConfig.getInstance(this).swipeGestures) return;
        if (canGoBack()) {
            goBack();
        }
    }

    private boolean canGoBack() {
        return this.mWebview.canGoBack();
    }

    private void goBack() {
        if (LeanWebView.isCrosswalk()) {
            // not safe to do for non-crosswalk, as we may never get a page finished callback
            // for single-page apps
            hideWebview();
        }

        this.mWebview.goBack();
    }

    public void sharePage(String optionalUrl) {
        String shareUrl;
        String currentUrl = this.mWebview.getUrl();
        if (optionalUrl == null || optionalUrl.isEmpty()) {
            shareUrl = currentUrl;
        } else {
            try {
                java.net.URI optionalUri = new java.net.URI(optionalUrl);
                if (optionalUri.isAbsolute()) {
                    shareUrl = optionalUrl;
                } else {
                    java.net.URI currentUri = new java.net.URI(currentUrl);
                    shareUrl = currentUri.resolve(optionalUri).toString();
                }
            } catch (URISyntaxException e) {
                shareUrl = optionalUrl;
            }
        }

        if (shareUrl == null || shareUrl.isEmpty()) return;

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, shareUrl);
        startActivity(Intent.createChooser(share, getString(R.string.action_share)));
    }

    private void logout() {
        this.mWebview.stopLoading();

        // log out by clearing all cookies and going to home page
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        CookieSyncManager.getInstance().sync();

        updateMenu(false);
        this.loginManager.checkLogin();
        this.mWebview.loadUrl(AppConfig.getInstance(this).initialUrl);
    }

    public void loadUrl(String url) {
        loadUrl(url, false);
    }

    public void loadUrl(String url, boolean isFromTab) {
        if (url == null) return;

        this.postLoadJavascript = null;
        this.postLoadJavascriptForRefresh = null;

        if (url.equalsIgnoreCase("gonative_logout"))
            logout();
        else
            this.mWebview.loadUrl(url);

        if (!isFromTab && this.tabManager != null) this.tabManager.selectTab(url, null);
    }

    public void loadUrlAndJavascript(String url, String javascript) {
        loadUrlAndJavascript(url, javascript, false);
    }

    public void loadUrlAndJavascript(String url, String javascript, boolean isFromTab) {
        String currentUrl = this.mWebview.getUrl();

        if (url != null && currentUrl != null && url.equals(currentUrl)) {
//            hideWebview();
            runJavascript(javascript);
            this.postLoadJavascriptForRefresh = javascript;
//            showWebview();
        } else {
            this.postLoadJavascript = javascript;
            this.postLoadJavascriptForRefresh = javascript;
            this.mWebview.loadUrl(url);
        }

        if (!isFromTab && this.tabManager != null) this.tabManager.selectTab(url, javascript);
    }

    public void runJavascript(String javascript) {
        if (javascript == null) return;
        this.mWebview.runJavascript(javascript);
    }
	
	public boolean isDisconnected(){
		NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni == null || !ni.isConnected();
	}

	public void clearWebviewCache() {
        mWebview.clearCache(true);
    }
	// configures webview settings
	private void setupWebview(GoNativeWebviewInterface wv){
        WebViewSetup.setupWebviewForActivity(wv, this);
	}

    private void showSplashScreen(double maxTime, double forceTime) {
        splashDialog = new Dialog(this, R.style.SplashScreen);
        if (splashDialog.getWindow() != null) {
            splashDialog.getWindow().getAttributes().windowAnimations = R.style.SplashScreenAnimation;
        }
        splashDialog.setContentView(R.layout.splash_screen);
        splashDialog.setCancelable(false);
        splashDialog.show();

        double delay;

        if (forceTime > 0) {
            delay = forceTime;
            splashDismissRequiresForce = true;
        } else {
            delay = maxTime;
            splashDismissRequiresForce = false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideSplashScreen(true);
            }
        }, (long) (delay * 1000));
    }

    private void hideSplashScreen(boolean isForce) {
        if (splashDialog != null && (!splashDismissRequiresForce || isForce)) {
            splashDialog.dismiss();
            splashDialog = null;
        }
    }

    public void hideWebview() {
        if (AppConfig.getInstance(this).disableAnimations) return;

        this.webviewIsHidden = true;
        mProgress.setAlpha(1.0f);
        mProgress.setVisibility(View.VISIBLE);

        if (this.isFirstHideWebview) {
            this.webviewOverlay.setAlpha(1.0f);
        } else {
            this.webviewOverlay.setAlpha(1 - this.hideWebviewAlpha);
        }

        showWebview(10);
    }

    private void showWebview(double delay) {
        hideSplashScreen(false);

        if (delay > 0) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showWebview();
                }
            }, (int) (delay * 1000));
        } else {
            showWebview();
        }
    }

    // shows webview with no animation
    public void showWebviewImmediately() {
        hideSplashScreen(false);

        this.isFirstHideWebview = false;
        webviewIsHidden = false;
        startedLoading = false;
        stopCheckingReadyStatus();
        this.webviewOverlay.setAlpha(0.0f);
        this.mProgress.setVisibility(View.INVISIBLE);

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            injectCSSviaJavascript();
        }
    }

    public void showWebview() {
        hideSplashScreen(false);

        this.isFirstHideWebview = false;
        startedLoading = false;
        stopCheckingReadyStatus();

        if (!webviewIsHidden) {
            // don't animate if already visible
            mProgress.setVisibility(View.INVISIBLE);
            return;
        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            injectCSSviaJavascript();
        }

        webviewIsHidden = false;

        webviewOverlay.animate().alpha(0.0f)
                .setDuration(300)
                .setStartDelay(150);

        mProgress.animate().alpha(0.0f)
                .setDuration(60);
    }

    private void injectCSSviaJavascript() {
        AppConfig appConfig = AppConfig.getInstance(this);
        if (appConfig.customCSS == null || appConfig.customCSS.isEmpty()) return;

        try {
            String encoded = Base64.encodeToString(appConfig.customCSS.getBytes("utf-8"), Base64.NO_WRAP);
            String js = "(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()";
            runJavascript(js);
        } catch (Exception e) {
            Log.e(TAG, "Error injecting customCSS via javascript", e);
        }
    }

    public void showLogoInActionBar(boolean show) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;

        actionBar.setDisplayOptions(show ? 0 : ActionBar.DISPLAY_SHOW_TITLE, ActionBar.DISPLAY_SHOW_TITLE);

        if (show) {
            // disable text title
            actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

            // why use a custom view and not setDisplayUseLogoEnabled and setLogo?
            // Because logo doesn't work!
            actionBar.setDisplayShowCustomEnabled(true);
            if (this.navigationTitleImage == null) {
                this.navigationTitleImage = new ImageView(this);
                this.navigationTitleImage.setImageResource(R.drawable.ic_actionbar);
            }
            actionBar.setCustomView(this.navigationTitleImage);
        } else {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE, ActionBar.DISPLAY_SHOW_TITLE);
            actionBar.setDisplayShowCustomEnabled(false);
        }
    }

	public void updatePageTitle() {
        if (AppConfig.getInstance(this).useWebpageTitle) {
            setTitle(this.mWebview.getTitle());
        }
    }

    public void update (Observable sender, Object data) {
        if (sender instanceof LoginManager) {
            updateMenu(((LoginManager) sender).isLoggedIn());
        }
    }

	public void updateMenu(){
        this.loginManager.checkLogin();
	}

    private void updateMenu(boolean isLoggedIn){
        if (menuAdapter == null)
            setupMenu();

        try {
            if (isLoggedIn)
                menuAdapter.update("loggedIn");
            else
                menuAdapter.update("default");
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

	private boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mDrawerView);
    }

    private void setDrawerEnabled(boolean enabled) {
        if (!isRoot) return;

        AppConfig appConfig = AppConfig.getInstance(this);
        if (!appConfig.showNavigationMenu) return;

        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                    DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(enabled);
            ColorDrawable colorDrawable
                    = new ColorDrawable(Color.parseColor("#415364"));

            // Set BackgroundDrawable
            actionBar.setBackgroundDrawable(colorDrawable);
        }
    }
	
	private void setupMenu(){
        menuAdapter = new JsonMenuAdapter(this);
        try {
            menuAdapter.update("default");
            mDrawerList.setAdapter(menuAdapter);
        } catch (Exception e) {
            Log.e(TAG, "Error setting up menu", e);
        }

        mDrawerList.setOnGroupClickListener(menuAdapter);
        mDrawerList.setOnChildClickListener(menuAdapter);
	}
	
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle != null)
		    mDrawerToggle.syncState();
	}
	
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
     // Pass any configuration change to the drawer toggles
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
	@Override
    @TargetApi(21)
    // Lollipop target API for REQEUST_SELECT_FILE_LOLLIPOP
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getBooleanExtra("exit", false))
            finish();

        String url = null;
        boolean success = false;
        if (data != null) {
            url = data.getStringExtra("url");
            success = data.getBooleanExtra("success", false);
        }

        if (requestCode == REQUEST_WEBFORM && resultCode == RESULT_OK) {
            if (url != null)
                loadUrl(url);
            else {
                // go to initialURL without login/signup override
                this.mWebview.setCheckLoginSignup(false);
                this.mWebview.loadUrl(AppConfig.getInstance(this).initialUrl);
            }

            if (AppConfig.getInstance(this).showNavigationMenu) {
                updateMenu(success);
            }
        }

        if (requestCode == REQUEST_WEB_ACTIVITY && resultCode == RESULT_OK) {
            if (url != null) {
                int urlLevel = data.getIntExtra("urlLevel", -1);
                if (urlLevel == -1 || parentUrlLevel == -1 || urlLevel > parentUrlLevel) {
                    // open in this activity
                    this.postLoadJavascript = data.getStringExtra("postLoadJavascript");
                    loadUrl(url);
                } else {
                    // urlLevel <= parentUrlLevel, so pass up the chain
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        }

        if (requestCode == REQUEST_SELECT_FILE) {
            if (resultCode != RESULT_OK) {
                cancelFileUpload();
                return;
            }

            // from documents (and video camera)
            if (data != null && data.getData() != null) {
                if (mUploadMessage != null) {
                    mUploadMessage.onReceiveValue(data.getData());
                    mUploadMessage = null;
                }

                if (uploadMessageLP != null) {
                    uploadMessageLP.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                    uploadMessageLP = null;
                }

                return;
            }

            // we may get clip data for multi-select documents
            if (data != null && data.getClipData() != null) {
                ClipData clipData = data.getClipData();
                ArrayList<Uri> files = new ArrayList<>(clipData.getItemCount());
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);
                    if (item.getUri() != null) {
                        files.add(item.getUri());
                    }
                }

                if (mUploadMessage != null) {
                    // shouldn never happen, but just in case, send the first item
                    if (files.size() > 0) {
                        mUploadMessage.onReceiveValue(files.get(0));
                    } else {
                        mUploadMessage.onReceiveValue(null);
                    }
                    mUploadMessage = null;
                }

                if (uploadMessageLP != null) {
                    uploadMessageLP.onReceiveValue(files.toArray(new Uri[files.size()]));
                    uploadMessageLP = null;
                }

                return;
            }

            // from camera
            if (this.directUploadImageUri != null) {
                // check if we have external storage permissions
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, R.string.external_storage_explanation, Toast.LENGTH_LONG).show();
                    }

                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_PERMISSION_READ_EXTERNAL_STORAGE);
                    // wait for the onRequestPermissionsResult callback
                    return;
                }


                if (mUploadMessage != null) {
                    mUploadMessage.onReceiveValue(this.directUploadImageUri);
                    mUploadMessage = null;
                }
                if (uploadMessageLP != null) {
                    uploadMessageLP.onReceiveValue(new Uri[]{this.directUploadImageUri});
                    uploadMessageLP = null;
                }
                this.directUploadImageUri = null;

                return;
            }

            // Should not reach here.
            cancelFileUpload();
        }
    }

    public void cancelFileUpload() {
        if (mUploadMessage != null) {
            mUploadMessage.onReceiveValue(null);
            mUploadMessage = null;
        }

        if (uploadMessageLP != null) {
            uploadMessageLP.onReceiveValue(null);
            uploadMessageLP = null;
        }

        this.directUploadImageUri = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        String url = getUrlFromIntent(intent);
        if (url != null && !url.isEmpty()){
            loadUrl(url);
        }
        Log.w(TAG, "Received intent without url");
    }

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (this.mWebview.exitFullScreen()) {
                return true;
            }

			if (isDrawerOpen()){
				mDrawerLayout.closeDrawers();
				return true;
			}
            else if (canGoBack()) {
                //goBack();
                runJavascript("window.history.back()");
                return true;
            }
            else if (!this.previousWebviewStates.isEmpty()) {
                Bundle state = previousWebviewStates.pop();
                LeanWebView webview = new LeanWebView(this);
                webview.restoreStateFromBundle(state);
                switchToWebview(webview, /* isPool */ false, /* isBack */ true);
                return true;
            }
		}

		return super.onKeyDown(keyCode, event);
	}

    // isPoolWebView is used to keep track of whether we are showing a pooled webview, which has implications
    // for page navigation, namely notifying the pool to disown the webview.
    // isBack means the webview is being switched in as part of back navigation behavior. If isBack=false,
    // then we will save the state of the old one switched out.
    public void switchToWebview(GoNativeWebviewInterface newWebview, boolean isPoolWebview, boolean isBack) {
        setupWebview(newWebview);

        // scroll to top
        ((View)newWebview).scrollTo(0, 0);

        View prev = (View)this.mWebview;

        if (!isBack) {
            // save the state for back button behavior
            Bundle stateBundle = new Bundle();
            this.mWebview.saveStateToBundle(stateBundle);
            this.previousWebviewStates.add(stateBundle);
        }

        // replace the current web view in the parent with the new view
        if (newWebview != prev) {
            // a view can only have one parent, and attempting to add newWebview if it already has
            // a parent will cause a runtime exception. So be extra safe by removing it from its parent.
            ViewParent temp = newWebview.getParent();
            if (temp instanceof  ViewGroup) {
                ((ViewGroup) temp).removeView((View)newWebview);
            }

            ViewGroup parent = (ViewGroup) prev.getParent();
            int index = parent.indexOfChild(prev);
            parent.removeView(prev);
            parent.addView((View) newWebview, index);
            ((View)newWebview).setLayoutParams(prev.getLayoutParams());

            // webviews can still send some extraneous events to this activity if we do not remove
            // its callbacks
            WebViewSetup.removeCallbacks((LeanWebView) prev);

            if (!this.isPoolWebview) {
                ((GoNativeWebviewInterface)prev).destroy();
            }

            ((GoNativeWebviewInterface)prev).setOnSwipeListener(null);
            if ((AppConfig.getInstance(this).swipeGestures)) {
                newWebview.setOnSwipeListener(this);
            }
        }

        this.isPoolWebview = isPoolWebview;
        this.mWebview = newWebview;

        if (this.postLoadJavascript != null) {
            runJavascript(this.postLoadJavascript);
            this.postLoadJavascript = null;
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.topmenu, menu);

        AppConfig appConfig = AppConfig.getInstance(this);

        // search item in action bar
		final MenuItem searchItem = menu.findItem(R.id.action_search);
        if (appConfig.searchTemplateUrl != null) {
            // make it visible
            searchItem.setVisible(true);

            final SearchView searchView = (SearchView) searchItem.getActionView();
            if (searchView != null) {
                SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
                if (searchAutoComplete != null) {
                    searchAutoComplete.setTextColor(appConfig.actionbarForegroundColor);
                    int hintColor = appConfig.actionbarForegroundColor;
                    hintColor = Color.argb(192, Color.red(hintColor), Color.green(hintColor),
                            Color.blue(hintColor));
                    searchAutoComplete.setHintTextColor(hintColor);
                }

                ImageView closeButtonImage = searchView.findViewById(androidx.appcompat.R.id.search_close_btn);
                if (closeButtonImage != null) {
                    closeButtonImage.setColorFilter(appConfig.actionbarForegroundColor);
                }


                // listener to process query
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchItem.collapseActionView();

                        try {
                            String q = URLEncoder.encode(query, "UTF-8");
                            loadUrl(AppConfig.getInstance(getApplicationContext()).searchTemplateUrl + q);
                        } catch (UnsupportedEncodingException e) {
                            return true;
                        }

                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        // do nothing
                        return true;
                    }
                });

                // listener to collapse action view when soft keyboard is closed
                searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            searchItem.collapseActionView();
                        }
                    }
                });
            }
        }

        if (!appConfig.showRefreshButton) {
            MenuItem refreshItem = menu.findItem(R.id.action_refresh);
            if (refreshItem != null) {
                refreshItem.setVisible(false);
            }
        }

        if (this.actionManager != null) {
            this.actionManager.addActions(menu);
        }

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event

        if (mDrawerToggle != null) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
              return true;
            }
        }

        // actions
        if (this.actionManager != null) {
            if (this.actionManager.onOptionsItemSelected(item)) {
                return true;
            }
        }
        
        // handle other items
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
	        case R.id.action_search:
	        	return true;
	        case R.id.action_refresh:
                onRefresh();
	        	return true;
        	default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        refreshPage();
        // let the refreshing spinner stay for a little bit if the native show/hide is disabled
        // otherwise there isn't enough of a user confirmation that the page is refreshing
        if (AppConfig.getInstance(this).disableAnimations) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeRefresh.setRefreshing(false);
                }
            }, 1000); // 1 second
        } else {
            this.swipeRefresh.setRefreshing(false);
        }
    }

    private void refreshPage() {
        String url = this.mWebview.getUrl();
        if (url != null && url.startsWith("file:///android_asset/offline")){
            if (this.mWebview.canGoBack()) {
                this.mWebview.goBack();
            } else if (this.initialUrl != null) {
                this.mWebview.loadUrl(this.initialUrl);
            }
            updateMenu();
        }
        else {
            this.postLoadJavascript = this.postLoadJavascriptForRefresh;
            this.mWebview.loadUrl(url);
        }
    }

    public void sendOneSignalInfo() {
        boolean doNativeBridge = true;
        String currentUrl = this.mWebview.getUrl();
        if (currentUrl != null) {
            doNativeBridge = LeanUtils.checkNativeBridgeUrls(currentUrl, this);
        }

        // onesignal javsacript callback
        if (AppConfig.getInstance(this).oneSignalEnabled && doNativeBridge) {
            try {
                String userId = null;
                String pushToken = null;
                boolean subscribed = false;

                OSPermissionSubscriptionState state = OneSignal.getPermissionSubscriptionState();
                if (state != null && state.getSubscriptionStatus() != null) {
                    userId = state.getSubscriptionStatus().getUserId();
                    pushToken = state.getSubscriptionStatus().getPushToken();
                    subscribed = state.getSubscriptionStatus().getSubscribed();
                }

                Map installationInfo = Installation.getInfo(this);

                JSONObject jsonObject = new JSONObject(installationInfo);
                if (userId != null) {
                    jsonObject.put("oneSignalUserId", userId);
                }
                if (pushToken != null) {
                    // registration id is old GCM name, but keep it for compatibility
                    jsonObject.put("oneSignalregistrationId", pushToken);
                    jsonObject.put("oneSignalPushToken", pushToken);
                }
                jsonObject.put("oneSignalSubscribed", subscribed);
                jsonObject.put("oneSignalRequiresUserPrivacyConsent", !OneSignal.userProvidedPrivacyConsent());
                String js = LeanUtils.createJsForCallback("gonative_onesignal_info", jsonObject);
                runJavascript(js);
            } catch (Exception e) {
                Log.e(TAG, "Error with onesignal javscript callback", e);
            }
        }
    }

    // onPageFinished
    public void checkNavigationForPage(String url) {
        // don't change anything on navigation if the url that just finished was a file download
        if (url.equals(this.fileDownloader.getLastDownloadedUrl())) return;

        if (this.tabManager != null) {
            this.tabManager.checkTabs(url);
        }

        if (this.actionManager != null) {
            this.actionManager.checkActions(url);
        }

        if (this.registrationManager != null) {
            this.registrationManager.checkUrl(url);
        }

        sendOneSignalInfo();
    }

    // onPageStarted
    public void checkPreNavigationForPage(String url) {
        if (this.tabManager != null) {
            this.tabManager.autoSelectTab(url);
        }

        AppConfig appConfig = AppConfig.getInstance(this);
        setDrawerEnabled(appConfig.shouldShowSidebarForUrl(url));
    }

    public int urlLevelForUrl(String url) {
        ArrayList<Pattern> entries = AppConfig.getInstance(this).navStructureLevelsRegex;
        if (entries != null) {
            for (int i = 0; i < entries.size(); i++) {
                Pattern regex = entries.get(i);
                if (regex.matcher(url).matches()) {
                    return AppConfig.getInstance(this).navStructureLevels.get(i);
                }
            }
        }

        // return unknown
        return -1;
    }

    public String titleForUrl(String url) {
        ArrayList<HashMap<String,Object>> entries = AppConfig.getInstance(this).navTitles;
        String title = null;

        if (entries != null) {
            for (HashMap<String,Object> entry : entries) {
                Pattern regex = (Pattern)entry.get("regex");

                if (regex.matcher(url).matches()) {
                    if (entry.containsKey("title")) {
                        title = (String)entry.get("title");
                    }

                    if (title == null && entry.containsKey("urlRegex")) {
                        Pattern urlRegex = (Pattern)entry.get("urlRegex");
                        Matcher match = urlRegex.matcher(url);
                        if (match.find() && match.groupCount() >= 1) {
                            String temp = match.group(1);
                            // dashes to spaces, capitalize
                            temp = temp.replace("-", " ");
                            temp = LeanUtils.capitalizeWords(temp);

                            title = temp;
                        }

                        // remove words from end of title
                        if (title != null && entry.containsKey("urlChompWords") &&
                                (Integer)entry.get("urlChompWords") > 0) {
                            int chompWords = (Integer)entry.get("urlChompWords");
                            String[] words = title.split("\\s+");
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < words.length - chompWords - 1; i++){
                                sb.append(words[i]);
                                sb.append(" ");
                            }
                            if (words.length > chompWords) {
                                sb.append(words[words.length - chompWords - 1]);
                            }
                            title = sb.toString();
                        }
                    }

                    break;
                }
            }
        }

        return title;
    }

    public void closeDrawers() {
        mDrawerLayout.closeDrawers();
    }

    public boolean isNotRoot() {
        return !isRoot;
    }

    public int getParentUrlLevel() {
        return parentUrlLevel;
    }

    public int getUrlLevel() {
        return urlLevel;
    }

    public void setUrlLevel(int urlLevel) {
        this.urlLevel = urlLevel;
    }

    public ProfilePicker getProfilePicker() {
        return profilePicker;
    }

    public FileDownloader getFileDownloader() {
        return fileDownloader;
    }

    public FileWriterSharer getFileWriterSharer() {
        return fileWriterSharer;
    }

    public BiometricBridge getBiometricBridge() {
        return new BiometricBridge();
    }

    public ToastBridge getToasterBridge() {
        return new ToastBridge();
    }

    public StatusCheckerBridge getStatusCheckerBridge() {
        return new StatusCheckerBridge();
    }

    public CallBridge getCallBridge(){ return new CallBridge();}

    public ActionViewBridge getActionViewBridge(){return new ActionViewBridge();}

    public MapsViewBridge getMapsViewBridge(){return  new MapsViewBridge();}

    public FBViewBridge getFBViewBridge(){return  new FBViewBridge();}

    public TwitterBridge getTwitterBridge() {return new TwitterBridge();}

    public  SendEmailBridge getSendEmailBridge(){return new SendEmailBridge();}

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void startCheckingReadyStatus() {
        statusChecker.run();
    }

    private void stopCheckingReadyStatus() {
        handler.removeCallbacks(statusChecker);
    }

    private void checkReadyStatus() {
        this.mWebview.runJavascript("if (gonative_status_checker && typeof gonative_status_checker.onReadyState === 'function') gonative_status_checker.onReadyState(document.readyState);");
    }

    private void checkReadyStatusResult(String status) {
        // if interactiveDelay is specified, then look for readyState=interactive, and show webview
        // with a delay. If not specified, wait for readyState=complete.
        double interactiveDelay = AppConfig.getInstance(this).interactiveDelay;

        if (status.equals("loading") || (Double.isNaN(interactiveDelay) && status.equals("interactive"))) {
            startedLoading = true;
        }
        else if ((!Double.isNaN(interactiveDelay) && status.equals("interactive"))
                || (startedLoading && status.equals("complete"))) {

            if (status.equals("interactive")) {
                showWebview(interactiveDelay);
            } else {
                showWebview();
            }
        }
    }

    public void showTabs() {
        this.slidingTabStrip.setVisibility(View.VISIBLE);
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
        }
        ViewCompat.setElevation(this.slidingTabStrip, ACTIONBAR_ELEVATION);
    }

    public void hideTabs() {
        this.slidingTabStrip.setVisibility(View.GONE);
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(ACTIONBAR_ELEVATION);
        }
    }

    public void toggleFullscreen(boolean fullscreen) {
        ActionBar actionBar = this.getSupportActionBar();
        View decorView = getWindow().getDecorView();
        int visibility = decorView.getSystemUiVisibility();
        int fullscreenFlags = View.SYSTEM_UI_FLAG_LOW_PROFILE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

        if (Build.VERSION.SDK_INT >= 16) {
            fullscreenFlags |= View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
        }

        if (Build.VERSION.SDK_INT >= 19) {
            fullscreenFlags |= View.SYSTEM_UI_FLAG_IMMERSIVE |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        if (fullscreen) {
            visibility |= fullscreenFlags;
            if (actionBar != null) actionBar.hide();
        } else {
            visibility &= ~fullscreenFlags;
            if (actionBar != null && AppConfig.getInstance(this).showActionBar) actionBar.show();

            // Fix for webview keyboard not showing, see https://github.com/mozilla-tw/FirefoxLite/issues/842
            this.mWebview.clearFocus();
        }

        decorView.setSystemUiVisibility(visibility);

        // Full-screen is used for playing videos.
        // Allow sensor-based rotation when in full screen (even overriding user rotation preference)
        if (fullscreen) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        } else {
            setScreenOrientationPreference();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (this.directUploadImageUri == null) {
                        cancelFileUpload();
                        return;
                    }

                    if (mUploadMessage != null) {
                        mUploadMessage.onReceiveValue(this.directUploadImageUri);
                        mUploadMessage = null;
                    }
                    if (uploadMessageLP != null) {
                        uploadMessageLP.onReceiveValue(new Uri[]{this.directUploadImageUri});
                        uploadMessageLP = null;
                    }

                    this.directUploadImageUri = null;
                } else {
                    cancelFileUpload();
                }
                break;
            case REQUEST_PERMISSION_GEOLOCATION:
                if (this.geolocationPermissionCallback != null) {
                    if (grantResults.length >= 2 &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        this.geolocationPermissionCallback.onResult(true);
                    } else {
                        this.geolocationPermissionCallback.onResult(false);
                    }
                    this.geolocationPermissionCallback = null;
                }
                break;
            case REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.fileDownloader.gotExternalStoragePermissions(true);
                }
                break;
            case REQUEST_PHONE_CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    String tel = "tel: " + phonenumber;
                    intent.setData(Uri.parse(tel));
                    try {
                        startActivity(intent);
                    }
                    catch (Exception ex){
                                   Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case REQUEST_PERMISSION_GENERIC:
                Iterator<PermissionsCallbackPair> it = pendingPermissionRequests.iterator();
                while (it.hasNext()) {
                    PermissionsCallbackPair pair = it.next();
                    if (pair.permissions.length != permissions.length) continue;
                    boolean skip = false;
                    for (int i = 0; i < pair.permissions.length && i < permissions.length; i++) {
                        if (!pair.permissions[i].equals(permissions[i])) {
                            skip = true;
                            break;
                        }
                    }
                    if (skip) continue;

                    // matches PermissionsCallbackPair
                    if (pair.callback != null) {
                        pair.callback.onPermissionResult(permissions, grantResults);
                    }
                    it.remove();
                }

                if (pendingPermissionRequests.size() == 0 && pendingStartActivityAfterPermissions.size() > 0) {
                    Iterator<Intent> i = pendingStartActivityAfterPermissions.iterator();
                    while (i.hasNext()) {
                        Intent intent = i.next();
                        startActivity(intent);
                        i.remove();
                    }
                }
                break;
        }
    }

    public void setUploadMessage(ValueCallback<Uri> mUploadMessage) {
        this.mUploadMessage = mUploadMessage;
    }

    public void setUploadMessageLP(ValueCallback<Uri[]> uploadMessageLP) {
        this.uploadMessageLP = uploadMessageLP;
    }

    public void setDirectUploadImageUri(Uri directUploadImageUri) {
        this.directUploadImageUri = directUploadImageUri;
    }

    public RelativeLayout getFullScreenLayout() {
        return fullScreenLayout;
    }


    // The javascript bridge to link the cuonline+ webview to the native biometric functionality
    // APP-31
    public class BiometricBridge{

        @JavascriptInterface
        public int CheckCipher()
        {
            Cipher cipher = getCipher();
            SecretKey secretKey = getSecretKey();

            if (secretKey == null) {
                return 0;
            }

            try {
                byte[] lastIv = getLastIv();
                cipher.init(Cipher.DECRYPT_MODE, secretKey,new IvParameterSpec(lastIv));
            }   catch ( InvalidKeyException | InvalidAlgorithmParameterException  e) {
                e.printStackTrace();

                return 0;
            }

            return 1;
        }


        @TargetApi(24)
        @JavascriptInterface
        public void showBiometric(){

            mode = "login";


            Context context = getApplicationContext();
            BiometricManager biometricManager = BiometricManager.from(context);
            switch (biometricManager.canAuthenticate()) {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                    Cipher cipher = getCipher();
                    SecretKey secretKey = getSecretKey();

                    if (secretKey == null) {
                        return;
                    }


                    try {
                        byte[] lastIv = getLastIv();
                        cipher.init(Cipher.DECRYPT_MODE, secretKey,new IvParameterSpec(lastIv));
                    }   catch ( InvalidKeyException | InvalidAlgorithmParameterException  e) {
                        e.printStackTrace();
                        String js = "gonative_invalidated_biometric()";
                        runJavascript(js);
                        return ;
                    }


                    promptInfo = new BiometricPrompt.PromptInfo.Builder()
                            .setTitle("Fingerprint Login")
                            .setSubtitle("Confirm fingerprint to login")
                            .setNegativeButtonText("Cancel")
                            .build();

                    biometricPrompt.authenticate(promptInfo,new BiometricPrompt.CryptoObject(cipher));

                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    Log.e("MY_APP_TAG", "No biometric features available on this device.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                            "any biometric credentials with their account.");
                    break;
            }
            return;
        }

        @TargetApi(24)
        @JavascriptInterface
        public void showBiometricRegister(String auserid, String acuid, String aPassword){

            mode = "reg";
            userid = auserid;
            cuid = acuid;
            password = aPassword;

            Context context = getApplicationContext();
            BiometricManager biometricManager = BiometricManager.from(context);
            switch (biometricManager.canAuthenticate()) {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.");

                    Cipher cipher = getCipher();
                    SecretKey secretKey = null;// =  getSecretKey();

                    if(secretKey == null)
                    {
                        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(
                                "cuMobileKey",
                                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                                .setUserAuthenticationRequired(true);

                            // Require the user to authenticate for every use of the key. This is the default, i.e.
                            // userAuthenticationValidityDurationSeconds is -1 unless specified otherwise.
                            // Explicitly setting it to -1 here for the sake of example.
                            // For this to work, at least one biometric must be enrolled.
                            builder.setUserAuthenticationValidityDurationSeconds(-1);

                            // This is a workaround to avoid crashes on devices whose API level is < 24
                            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
                            // visible on API level 24+.
                            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
                            // it isn't available yet.
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                // Invalidate the keys if a new biometric has been enrolled.
                                builder.setInvalidatedByBiometricEnrollment(true);
                            }


                        KeyGenerator keyGenerator = null;
                        try {
                            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (NoSuchProviderException e) {
                            e.printStackTrace();
                        }
                        try {
                            keyGenerator.init(builder.build());
                        } catch (InvalidAlgorithmParameterException e) {
                            e.printStackTrace();
                            return;
                        }

                        // Generates and stores the key in Android KeyStore under the keystoreAlias (keyName)
                        // specified in the builder.
                        keyGenerator.generateKey();


                        secretKey = getSecretKey();
                    }

                    try {
                        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

                        byte[] iv = cipher.getIV();
                        saveIv(iv);


                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                        return;
                    }

                    promptInfo = new BiometricPrompt.PromptInfo.Builder()
                            .setTitle("Register for fingerprint login")
                            .setSubtitle("Confirm fingerprint to register")
                            .setNegativeButtonText("Cancel")
                            .build();

                    // Prompt appears when user clicks "Log in".
                    biometricPrompt.authenticate(promptInfo,new BiometricPrompt.CryptoObject(cipher));

                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    Log.e("MY_APP_TAG", "No biometric features available on this device.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                            "any biometric credentials with their account.");
                    break;
            }
        }

        @JavascriptInterface
        public void testBiometric(){

            Context context = getApplicationContext();
            BiometricManager biometricManager = BiometricManager.from(context);
            switch (biometricManager.canAuthenticate()) {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.");


                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    Log.e("MY_APP_TAG", "No biometric features available on this device.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                            "any biometric credentials with their account.");
                    break;
            }
        }

        public void showPrompt(){

        }


    }


    public class ToastBridge{

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(String toast) {
            Context context = getApplicationContext();

            Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
        }
    }


    public class CallBridge{

        /** Show a toast from the web page */
        @JavascriptInterface
        public void makeCall(String phoneNumber) {
            Context context = getApplicationContext();
            phonenumber = phoneNumber;
            getMakeCallPermission();


        }
    }


    public class ActionViewBridge{

        /** Used to call an ACTION_VIEW Intent for a specified URI*/
        @JavascriptInterface
        public void displayActionView(String uri) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            if(intent != null)
            {
                try{
                    startActivity(intent);
                }
                catch (Exception ex)
                {
                    Toast.makeText (MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

        }
    }

    /** Class to show google maps with ATMS marked */
    public class MapsViewBridge
    {
        @JavascriptInterface
        public void displayGoogleMaps(String uri){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            if(intent != null)
            {
                try{
                    startActivity(intent);
                }
                catch (Exception ex)
                {
                    Toast.makeText (MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public class  TwitterBridge
    {
        @JavascriptInterface
        public void displayTwitterFeed(String userid) {

            Context context = getApplicationContext();
            Intent intent;
            String Url = "";


            try{
                // get the Twitter app if possible
                context.getPackageManager().getPackageInfo("com.twitter.android", 0);
                Url = "twitter://user?screen_name=" + userid;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            }
            catch(PackageManager.NameNotFoundException ex)
            {
                // no Twitter app, revert to browser
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"+userid));

            }

            try{
                startActivity(intent);
            }
            catch (Exception ex)
            {
                Toast.makeText (MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

   /** Class to launch cu facebook page, if facebook app is installed it will launch that  otherwise falls back to webpage*/
    public  class  FBViewBridge
    {
        @JavascriptInterface
        public void displayFaceBookPage(String uri){

            Context context = getApplicationContext();
            String Url = "";

            PackageManager packageManager =  context.getPackageManager();

            try{
                int versionCode = packageManager.getPackageInfo("com.facebook.katana",0).versionCode;
                boolean activated  = packageManager.getApplicationInfo("com.facebook.katana",0).enabled;

                if(activated)
                {
                    if(versionCode >= 3002850){
                        Url = "fb://facewebmodal/f?href=https://www.facebook.com/" + uri;
                    }
                    else{
                        Url ="fb://page/" + uri;
                    }
                }
                else
                {
                    Url = "https://www.facebook.com/" + uri;
                }

            }
            catch(PackageManager.NameNotFoundException ex)
            {
                Url = "https://www.facebook.com/" + uri;
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Url));
            //intent.setPackage("com.facebook.katana");
            if(intent != null)
            {
                try{
                    startActivity(intent);
                }
                catch (Exception ex)
                {
                    Toast.makeText (MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public class SendEmailBridge
    {
        @JavascriptInterface
        public void sendEmail(String recipients)
        {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL,new String[] { recipients });
            intent.putExtra(Intent.EXTRA_SUBJECT,"Member Email");

            if(intent != null)
            {
                try{
                    startActivity(Intent.createChooser(intent, "Send mail using..."));
                   // startActivity(intent);
                }
                catch (Exception ex)
                {
                    Toast.makeText (MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }
    }
    public class StatusCheckerBridge {
        @JavascriptInterface
        public void onReadyState(final String state) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkReadyStatusResult(state);
                }
            });
        }
    }

    private class ConnectivityChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            retryFailedPage();
            if (connectivityCallback != null) {
                sendConnectivity(connectivityCallback);
            }
        }
    }

    public void getRuntimeGeolocationPermission(final GeolocationPermissionCallback callback) {
        int checkFine = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int checkCoarse = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);

        if (checkFine == PackageManager.PERMISSION_GRANTED && checkCoarse == PackageManager.PERMISSION_GRANTED) {
            callback.onResult(true);
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Toast.makeText(this, R.string.request_permission_explanation_geolocation, Toast.LENGTH_SHORT).show();
        }

        this.geolocationPermissionCallback = callback;
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
        }, REQUEST_PERMISSION_GEOLOCATION);
    }

    public void getExternalStorageWritePermission() {
        // check external storage permission
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, R.string.request_permission_explanation_storage, Toast.LENGTH_LONG).show();
            }

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
        } else {
            this.fileDownloader.gotExternalStoragePermissions(true);
        }
    }

    public void getMakeCallPermission() {
        // check call permssions
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {
                Toast.makeText(this, R.string.request_permission_call_phone, Toast.LENGTH_LONG).show();
            }

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_PHONE_CALL);
        }
        else{
            Intent intent = new Intent(Intent.ACTION_CALL);
            String tel = "tel: " + phonenumber;
            intent.setData(Uri.parse(tel));
            try {
                startActivity(intent);
            }
            catch (Exception ex){
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void getPermission(String[] permissions, PermissionCallback callback) {
        boolean needToRequest = false;
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                needToRequest = true;
                break;
            }
        }

        if (needToRequest) {
            if (callback != null) {
                pendingPermissionRequests.add(new PermissionsCallbackPair(permissions, callback));
            }

            ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_GENERIC);
        } else {
            // send all granted result
            if (callback != null) {
                int[] results = new int[permissions.length];
                for (int i = 0; i < results.length; i++) {
                    results[i] = PackageManager.PERMISSION_GRANTED;
                }
                callback.onPermissionResult(permissions, results);
            }
        }
    }

    public void startActivityAfterPermissions(Intent intent) {
        if (pendingPermissionRequests.size() == 0) {
            startActivity(intent);
        } else {
            pendingStartActivityAfterPermissions.add(intent);
        }
    }

    private void setScreenOrientationPreference() {
        AppConfig appConfig = AppConfig.getInstance(this);
        if (appConfig.forceScreenOrientation == null) return;

        switch (appConfig.forceScreenOrientation) {
            case UNSPECIFIED:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                break;
            case PORTRAIT:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case LANDSCAPE:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                break;
        }
    }

    public TabManager getTabManager() {
        return tabManager;
    }

    public interface PermissionCallback {
        void onPermissionResult(String[] permissions, int[] grantResults);
    }

    private class PermissionsCallbackPair {
        String[] permissions;
        PermissionCallback callback;

        PermissionsCallbackPair(String[] permissions, PermissionCallback callback) {
            this.permissions = permissions;
            this.callback = callback;
        }
    }

    public void enableSwipeRefresh() {
        if (this.swipeRefresh != null) {
            this.swipeRefresh.setEnabled(true);
        }
    }

    public void restoreSwipRefreshDefault() {
        if (this.swipeRefresh != null) {
            AppConfig appConfig = AppConfig.getInstance(this);
            this.swipeRefresh.setEnabled(appConfig.pullToRefresh);
        }
    }

    public void deselectTabs() {
        this.slidingTabStrip.deselect();
    }

    private void listenForSignalStrength() {
        if (this.phoneStateListener != null) return;

        this.phoneStateListener = new PhoneStateListener() {
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                latestSignalStrength = signalStrength;
                sendConnectivityOnce();
                if (connectivityCallback != null) {
                    sendConnectivity(connectivityCallback);
                }
            }
        };

        try {
            TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null) {
                Log.e(TAG, "Error getting system telephony manager");
            } else {
                telephonyManager.listen(this.phoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error listening for signal strength", e);
        }

    }

    public void sendConnectivityOnce(String callback) {
        if (callback == null) return;

        this.connectivityOnceCallback = callback;
        if (this.phoneStateListener != null) {
            sendConnectivity(callback);
        } else {
            listenForSignalStrength();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendConnectivityOnce();
                }
            }, 500);
        }
    }

    private void sendConnectivityOnce() {
        if (this.connectivityOnceCallback == null) return;
        sendConnectivity(this.connectivityOnceCallback);
        this.connectivityOnceCallback = null;
    }

    private void sendConnectivity(String callback) {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean connected = activeNetwork != null && activeNetwork.isConnected();
        String typeString;
        if (activeNetwork != null) {
            typeString = activeNetwork.getTypeName();
        } else {
            typeString = "DISCONNECTED";
        }

        try {
            JSONObject data = new JSONObject();
            data.put("connected", connected);
            data.put("type", typeString);

            if (this.latestSignalStrength != null) {
                JSONObject signalStrength = new JSONObject();

                signalStrength.put("cdmaDbm", latestSignalStrength.getCdmaDbm());
                signalStrength.put("cdmaEcio", latestSignalStrength.getCdmaEcio());
                signalStrength.put("evdoDbm", latestSignalStrength.getEvdoDbm());
                signalStrength.put("evdoEcio", latestSignalStrength.getEvdoEcio());
                signalStrength.put("evdoSnr", latestSignalStrength.getEvdoSnr());
                signalStrength.put("gsmBitErrorRate", latestSignalStrength.getGsmBitErrorRate());
                signalStrength.put("gsmSignalStrength", latestSignalStrength.getGsmSignalStrength());
                if (Build.VERSION.SDK_INT >= 23) {
                    signalStrength.put("level", latestSignalStrength.getLevel());
                }
                data.put("cellSignalStrength", signalStrength);
            }

            String js = LeanUtils.createJsForCallback(callback, data);
            runJavascript(js);
        } catch (JSONException e) {
            Log.e(TAG, "JSON error sending connectivity", e);
        }
    }

    public void subscribeConnectivity(final String callback) {
        this.connectivityCallback = callback;
        listenForSignalStrength();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendConnectivity(callback);
            }
        }, 500);
    }

    public void unsubscribeConnectivity() {
        this.connectivityCallback = null;
    }

    public interface GeolocationPermissionCallback {
        void onResult(boolean granted);
    }

    // set brightness to a negative number to restore default
    public void setBrightness(float brightness) {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = brightness;
        getWindow().setAttributes(layout);
    }


    public class AppLifecycleObserver implements LifecycleObserver {

        public  final String TAG = AppLifecycleObserver.class.getName();

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onEnterForeground() {
            //run the code we need
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onEnterBackground() {
            //run the code we need
        }

    }
}
